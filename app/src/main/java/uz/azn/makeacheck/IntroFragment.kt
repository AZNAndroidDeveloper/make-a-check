package uz.azn.makeacheck

import android.graphics.*
import android.graphics.pdf.PdfDocument
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import uz.azn.makeacheck.databinding.FragmentIntroBinding
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class IntroFragment : Fragment(R.layout.fragment_intro) {
    private var count = 0
    private lateinit var bitmap: Bitmap
    private lateinit var scaledBitmap: Bitmap // yani bu rasmni razmerini ozgartirish uchun ishlatiladi
    private var pageWidth = 1200
    private val prices = intArrayOf(0, 21000, 18000, 17000)
    private lateinit var binding: FragmentIntroBinding
    private val elements = mutableListOf<Product>()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentIntroBinding.bind(view)

        bitmap = BitmapFactory.decodeResource(
            resources,
            R.drawable.evos_image
        ) /// drawable dan oqib oladi
        scaledBitmap =
            Bitmap.createScaledBitmap(bitmap, pageWidth, 510, false) // bu rasmi joylashtiryapti

        with(binding) {
            btnAdd.setOnClickListener {
                count++
                addFood(count)
            }

            btnSave.setOnClickListener {
                if (
                    etName.text.toString().isNotEmpty()
                    && etPhoneNumber.text.toString().isNotEmpty()
                    && checkValidation()
                ) {
                    Toast.makeText(requireContext(), "Checkni oling", Toast.LENGTH_SHORT).show()
                    val pdfDocument = PdfDocument()
                    val titlePaint = Paint() // bu chizish uchun ishlatiladi
                    val paint = Paint() // tana qismi uchun
                    val pageInfo =
                        PdfDocument.PageInfo.Builder(pageWidth, 2010, 1).create() // bu pdf yaratadi
                    val myPage = pdfDocument.startPage(pageInfo)
                    val canvas: Canvas = myPage.canvas
                    canvas.drawBitmap(scaledBitmap, 0f, 0f, paint) // rasmi joylashtirdik
                    paint.color = Color.WHITE
                    paint.textSize = 35f
                    paint.textAlign = Paint.Align.RIGHT
                    canvas.drawText(
                        "+998990060155",
                        pageWidth.toFloat() - 40,
                        50f,
                        paint
                    ) // bu bizga joylashtirib beradi

                    titlePaint.textAlign = Paint.Align.CENTER
                    titlePaint.typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD)
                    titlePaint.textSize = 65f
                    titlePaint.color = Color.BLACK
                    canvas.drawText("Hisobot:", (pageWidth / 2).toFloat(), 500f, titlePaint)

                    // chap qismi
                    paint.textAlign = Paint.Align.LEFT
                    paint.color = Color.BLACK
                    canvas.drawText("Mijoz: ${etName.text.toString()}", 20f, 590f, paint)
                    canvas.drawText("Tel: ${etPhoneNumber.text.toString()}", 20f, 640f, paint)

                    // ong qismi
                    paint.textAlign = Paint.Align.RIGHT

                    canvas.drawText(
                        "Buyurtma kodi ${UUID.randomUUID().toString().subSequence(0, 5)}",
                        pageWidth.toFloat() - 20f,
                        590f
                        , paint
                    )

                    //Buyurtma sanasi
                    val date = Date()
                    var dateFormat = SimpleDateFormat("dd/MM/yy")
                    canvas.drawText(
                        "Sana ${dateFormat.format(date)}",
                        pageWidth.toFloat() - 20f,
                        640f,
                        paint
                    )
                    // buyurtma vaqti
                    dateFormat = SimpleDateFormat("HH:MM")
                    canvas.drawText("Vaqt ${dateFormat.format(date)}", pageWidth - 20f, 690f, paint)

                    // line chizish
                    paint.style = Paint.Style.STROKE
                    paint.strokeWidth = 2f
                    canvas.drawRect(20f, 780f, pageWidth.toFloat() - 20, 860f, paint)

                    paint.textAlign = Paint.Align.LEFT
                    paint.style = Paint.Style.FILL
                    canvas.drawText("No.", 40f, 830f, paint)
                    canvas.drawText("Maxsulot", 200f, 830f, paint)
                    canvas.drawText("Narxi", 700f, 830f, paint)
                    canvas.drawText("Soni", 900f, 830f, paint)
                    canvas.drawText("Jami", 1050f, 830f, paint)

                    // Orasiga chiziq chizish Midle line

                    canvas.drawLine(180f, 790f, 180f, 850f, paint)
                    canvas.drawLine(680f, 790f, 680f, 850f, paint)
                    canvas.drawLine(880f, 790f, 880f, 850f, paint)
                    canvas.drawLine(1030f, 790f, 1030f, 850f, paint)
                    var y = 950f
                    var allSum = 0
                    for (i in 0 until elements.size) {
                        canvas.drawText("${i + 1}", 40f, y, paint)
                        Log.d("List", "onViewCreated:$i -> ${elements[i].productName}")
                        when (elements[i].productName.toString()) {
                            "Lavash" -> {
                                canvas.drawText("Lavash", 200f, y, paint)
                                canvas.drawText("${prices[1]}", 700f, y, paint)
                                canvas.drawText(
                                    ("${elements[i].count.toInt() * prices[1].toInt()}"),
                                    1050f,
                                    y,
                                    paint
                                )
                                allSum += elements[i].count.toInt() * prices[1]
                            }
                            "Donor" -> {
                                canvas.drawText("Donor", 200f, y, paint)
                                canvas.drawText("${prices[2]}", 700f, y, paint)
                                canvas.drawText(
                                    ("${elements[i].count.toInt() * prices[2].toInt()}"),
                                    1050f,
                                    y,
                                    paint
                                )
                                allSum += elements[i].count.toInt() * prices[2]

                            }
                            "Burger" -> {
                                canvas.drawText("Burger", 200f, y, paint)
                                canvas.drawText("${prices[3]}", 700f, y, paint)
                                canvas.drawText(
                                    ("${elements[i].count.toInt() * prices[3].toInt()}"),
                                    1050f,
                                    y,
                                    paint
                                )
                                allSum += elements[i].count.toInt() * prices[3]


                            }

                        }
                        canvas.drawText(elements[i].count, 900f, y, paint)

                        y += 100f

                    }

                    // final calculation

                    Log.d("y oqi", "onViewCreated: $y")
                    canvas.drawLine(680f,y+60f,pageWidth.toFloat()-20,y+60f,paint)
                    canvas.drawText("Jami " ,700f,y+120f,paint)
                    canvas.drawText(" : ",900f,y+120f,paint)
                    paint.textAlign = Paint.Align.RIGHT
                    canvas.drawText(allSum.toString(),pageWidth.toFloat()-40,y+120f,paint)

                     canvas.drawText("10%",780f,y+180f,paint)
                    canvas.drawText(" : ",926f,y+180f,paint)
                    paint.textAlign = Paint.Align.RIGHT
                    canvas.drawText("${allSum/100*10}",pageWidth.toFloat()-40,y+180,paint)

                    paint.textAlign = Paint.Align.LEFT
                    paint.color =Color.rgb(0,161,84)

                    canvas.drawRect(680f,y+200f,pageWidth.toFloat()-20,y+320f,paint)

                    paint.color =Color.WHITE
                     paint.textSize = 50f
                    paint.textAlign = Paint.Align.LEFT

                    canvas.drawText("To'lov",700f,y+270f, paint)
                    canvas.drawText(":",900f,y+270f, paint)
                    paint.textAlign = Paint.Align.RIGHT

                    canvas.drawText((allSum+allSum/100*10).toString(),pageWidth-40f,y+270f,paint)


                    pdfDocument.finishPage(myPage)
                    val file = File(
                        Environment.getExternalStorageDirectory(),
                        "/Mahsulot Checki${UUID.randomUUID().toString().subSequence(0, 5)}.pdf"
                    )
                    try {
                        pdfDocument.writeTo(FileOutputStream(file))
                    } catch (ex: IOException) {
                        ex.printStackTrace()
                    }
                    pdfDocument.close()

                }
            }
        }

    }


    private fun addFood(num: Int) {
        val view = layoutInflater.inflate(R.layout.products_view_holder, null, false)
        val foodNumber = view.findViewById<TextView>(R.id.food_number).setText(num.toString())
        val spinner = view.findViewById<Spinner>(R.id.spinner)
        val count = view.findViewById<EditText>(R.id.product_number)
        binding.linear.addView(view)


    }

    private fun checkValidation(): Boolean {
        elements.clear()
        var result = true
        var product = Product()
        with(binding) {
            for (i in 0 until linear.childCount) {

                val view = linear.getChildAt(i)
                val spinner = view.findViewById<Spinner>(R.id.spinner)
                val productNum = view.findViewById<EditText>(R.id.product_number)

                if (productNum.text.toString().isNotEmpty() && spinner.selectedItemPosition != 0) {
                    val selectedItem = spinner.selectedItem.toString()
                    Log.d("product", "checkValidation: $selectedItem")

                    elements.add(Product(selectedItem, productNum.text.toString()))
                    Log.d("element", "checkValidation: ${elements[i].productName}")
                } else
                    result = false


            }

            if (elements.isEmpty()) {
                result = false
                Toast.makeText(requireContext(), "Iltimos food qoshing", Toast.LENGTH_SHORT).show()
            } else if (!result) {
                Toast.makeText(requireContext(), "Malumotlarni to'liq kirting", Toast.LENGTH_SHORT)
                    .show()
            }
        }

        return result
    }


}